# ----------
# User Instructions:
#
# Modify the the search function so that it returns
# a shortest path as follows:
# 
# [['>', 'v', ' ', ' ', ' ', ' '],
#  [' ', '>', '>', '>', '>', 'v'],
#  [' ', ' ', ' ', ' ', ' ', 'v'],
#  [' ', ' ', ' ', ' ', ' ', 'v'],
#  [' ', ' ', ' ', ' ', ' ', '*']]
#
# Where '>', '<', '^', and 'v' refer to right, left, 
# up, and down motions. NOTE: the 'v' should be 
# lowercase.
#
# Your function should be able to do this for any
# provided grid, not just the sample grid below.
# ----------


# Sample Test case
grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost = 1

# ----------------------------------------
# modify code below
# ----------------------------------------

def search():
    index = 0
    expand = [[-1 for row in range(len(grid[0]))] for col in range(len(grid))]
    expand[init[0]][init[1]] = index

    x = init[0]
    y = init[1]
    g = 0

    open = [[g, x, y]]

    found = False  # flag that is set when search is complete
    resign = False # flag set if we can't find expand

    while not found and not resign:
        if len(open) == 0:
            resign = True
            return 'fail'
        else:
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[1]
            y = next[2]
            g = next[0]
            
            if x == goal[0] and y == goal[1]:
                found = True
            else:
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >=0 and y2 < len(grid[0]):
                        if expand[x2][y2] == -1 and grid[x2][y2] == 0:
                            g2 = g + cost
                            open.append([g2, x2, y2])
                            expand[x2][y2] = g2
    path = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]
    waypt = goal
    distance = expand[waypt[0]][waypt[1]]
    path[waypt[0]][waypt[1]] = "*"
    while distance > 0:
        dilated = [([a-b for a,b in zip(waypt,move)],name) for move, name in zip(delta,delta_name)]
        dilated = [pos for pos in dilated if (pos[0][0]>=0 and pos[0][0]<len(grid) and pos[0][1]>=0 and pos[0][1]<len(grid[0]))]
        dilated = [pos for pos in dilated if expand[pos[0][0]][pos[0][1]] > -1]
        dilated.sort(key= lambda pos : expand[pos[0][0]][pos[0][1]])
        nextpt= dilated[0]
        waypt = nextpt[0]
        path[waypt[0]][waypt[1]] = nextpt[1]
        distance = expand[waypt[0]][waypt[1]]
    for i in range(len(path)):
        print path[i]
    return path # make sure you return the shortest path.

search()