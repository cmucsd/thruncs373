# ----------
# User Instructions:
# 
# Create a function compute_value() which returns
# a grid of values. Value is defined as the minimum
# number of moves required to get from a cell to the
# goal. 
#
# If it is impossible to reach the goal from a cell
# you should assign that cell a value of 99.

# ----------

grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost_step = 1 # the cost associated with moving from a cell to an adjacent one.

# ----------------------------------------
# insert code below
# ----------------------------------------

# set obstacles to 99
def gridscale(g, add):
    return [[v+add for v in row] for row in g]

def gridadjust(g):
    for row in range(len(g)):
        for col in range(len(g[0])):
            if g[row][col] == 98:
                g[row][col]= 99

# get a set of admissible locations
def locations(g, origin):
    loci= [[o-d for o,d in zip(origin,move)] for move in delta]
    return [loc for loc in loci if (0 <= loc[0] and 0 <= loc[1] and loc[0] < len(g) and loc[1] < len(g[0]))]

def tag(g, origin, d):
    loci= locations(g,origin)
    loci= [loc for loc in loci if (g[loc[0]][loc[1]] < 99 and g[loc[0]][loc[1]]) > d]
#    loci.sort(key= lambda loc: g[loc[0]][loc[1]])
    for loc in loci:
        g[loc[0]][loc[1]]= d
    for loc in loci:
        tag(g, loc, d+cost_step) 

def compute_value():
    value= gridscale(grid, 98)
    d= 0
    value[goal[0]][goal[1]]= d
    tag(value, goal, d+cost_step) 
    gridadjust(value)
    return value #make sure your function returns a grid of values as demonstrated in the previous video.

print compute_value()
