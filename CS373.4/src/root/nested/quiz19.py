# ----------
# User Instructions:
# 
# Implement the function optimum_policy2D() below.
#
# You are given a car in a grid with initial state
# init = [x-position, y-position, orientation]
# where x/y-position is its position in a given
# grid and orientation is 0-3 corresponding to 'up',
# 'left', 'down' or 'right'.
#
# Your task is to compute and return the car's optimal
# path to the position specified in `goal'; where
# the costs for each motion are as defined in `cost'.

# EXAMPLE INPUT:

# grid format:
#     0 = navigable space
#     1 = occupied space 
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]
goal = [2, 0] # final position
init = [4, 3, 0] # first 2 elements are coordinates, third is direction
cost = [2, 1, 20] # the cost field has 3 values: right turn, no turn, left turn

# EXAMPLE OUTPUT:
# calling optimum_policy2D() should return the array
# 
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
#
# ----------


# there are four motion directions: up/left/down/right
# increasing the index in this array corresponds to
# a left turn. Decreasing is is a right turn.

forward = [[-1,  0], # go up
           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # do right
forward_name = ['up', 'left', 'down', 'right']

# the cost field has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']


# ----------------------------------------
# modify code below
# ----------------------------------------

def on_road(g, pos):
    x= pos[0]
    y= pos[1]
    return (x>=0 and y>=0 and x<len(g) and y<len(g[0]) and g[x][y]==0)

def positions(g, pos):
    olen= len(forward)
    [x, y, o]= pos
    diridx= [(o+a)%olen for a in action]
    posit= [[p+d for p,d in zip (pos[:2],forward[dir])]+[dir] for dir in diridx]
    return [[p, c, a] for p, c, a in zip(posit,cost,action_name) if on_road(g,p)]

def next_position(pol, pos):
    olen= len(forward)
    [x, y, o]= pos
    aname= pol[x][y][o]
    if aname in action_name:
        for act in range(len(action_name)):
            if aname == action_name[act]:
                break
    else:
        act= -1
    if act<0:
        nextpos=[-1,-1,-1]
    else:
        a= action[act]
        aidx= (o+a)%olen
        nextpos= [p+d for p,d in zip (pos[:2],forward[aidx])]+[aidx]
    return nextpos

def optimum_policy2D():
    value= [[[999 for hdg in range(len(forward))] for pos in range(len(grid[0]))] for row in range(len(grid))]
    policy= [[[' ' for hdg in range(len(forward))] for pos in range(len(grid[0]))] for row in range(len(grid))]
    policy2D= [[' ' for pos in range(len(grid[0]))] for row in range(len(grid))]
    [x,y] = goal
    for dir in range(len(forward)):
        value[x][y][dir]= 0
        policy[x][y][dir]= '*'
    changed= True
    while changed:
        changed= False
        for x in range(len(value)):
            for y in range(len(value[0])):
                if 0 == grid[x][y]:
                    for dir in range(len(value[0][0])):
                        pos= positions(grid, [x,y,dir])
                        for pa in pos:
                            [[xx, yy, dd], movecost, action]= pa
                            newval= value[xx][yy][dd]+movecost
                            if value[x][y][dir] > newval:
                                value[x][y][dir]= newval
                                policy[x][y][dir]= action
                                changed= True
    [x,y,o]= init
    while policy[x][y][o] != '*':
        policy2D[x][y]= policy[x][y][o]
        [x,y,o]= next_position(policy, [x,y,o])
        if [-1,-1,-1] == [x,y,o]:
            return policy2D
    policy2D[x][y]= '*'
    return policy2D # Make sure your function returns the expected grid.

optpol= optimum_policy2D()
for row in optpol:
    print row

