# ----------
# User Instructions:
# 
# Define a function, search() that takes no input
# and returns a list
# in the form of [optimal path length, x, y]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1] # Make sure that the goal definition stays in the function.

delta = [[-1, 0 ], # go up
        [ 0, -1], # go left
        [ 1, 0 ], # go down
        [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost = 1

def search():
    occupied = grid
    length = 0
    testpos = [init]
    occupied[init[0]][init[1]] = -1
    if init == goal:
        return [length, init[1], init[0]]
    while True:
        fail = True
        nextpos = []
        length += 1
        for tpos in testpos:
            dilated = [[a+b for a,b in zip(tpos,move)] for move in delta]
            dilated = [pos for pos in dilated if (pos[0]>=0 and pos[0]<len(grid) and pos[1]>=0 and pos[1]<len(grid[0]))]
            dilated = [pos for pos in dilated if occupied[pos[0]][pos[1]] == 0]
            for pos in dilated:
                occupied[pos[0]][pos[1]] = -1
                fail = False
                nextpos.append(pos)
                if pos == goal:
                    return [length, pos[0], pos[1]]
        testpos = nextpos
        if fail:
            return 'fail'

print search()
