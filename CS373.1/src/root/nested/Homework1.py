# Specification (DO NOT COPY)
colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

measurements = ['green', 'green', 'green' ,'green', 'green']


motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]

sensor_right = 0.7

p_move = 0.8

def show(p):
    for i in range(len(p)):
        print p[i]

#DO NOT USE IMPORT
#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

def recursive_sum(tensor):
    accu= 0
    for element in tensor:
        if isinstance(element, list):
            accu= accu + recursive_sum(element)
        else:
            accu= accu + element
    return accu

def recursive_map(function,tensor):
    if isinstance(tensor, list):
        ergebnis= map(lambda (element) : recursive_map(function, element), tensor)
    else:
        ergebnis= function(tensor)
    return ergebnis

def normalize(unnormal):
    skala = 1./recursive_sum(unnormal)
    normal=recursive_map(lambda(gewicht): skala*gewicht, unnormal)
    return normal

def vergleich(oans,zwoa,bsuffa):
    if oans == zwoa:
        faktor= bsuffa
    else:
        faktor= (1-bsuffa)
    return faktor

def sense(p, Z):
    ergebnis=[[gewicht*vergleich(farbe,Z,sensor_right) for (gewicht,farbe) in zip(gewichte,farben)] for (gewichte,farben) in zip(p,colors)]
    ergebnis= normalize(ergebnis)
    return ergebnis

def moveexact(p, U):
    bewegung= -U[0] % len(p)
    q= p[bewegung:]+p[:bewegung]
    if len(U[1:])>0:
        r=[moveexact(zeile,U[1:]) for zeile in q]
    else:
        r=q
    return r

def move(p, U):
    q=moveexact(p, U)
    r=[[p_move*b+(1-p_move)*u for (b,u) in zip(bewegt,unbewegt)] for (bewegt,unbewegt) in zip(q,p)]
    return r

def move_then_sense(p, s_and_m):
    q= sense(move(p, s_and_m[1]), s_and_m[0])
    return q

def uniformdistro(h,v):
    p= normalize([[1 for x in range(h)] for y in range(v)])
    return p

def localize(colors,sensor_right,p_move, p, messungen, bewegungen):
    q= reduce(move_then_sense, zip(messungen, bewegungen),p)
    return q

p = uniformdistro(len(colors[0]),len(colors))
p = localize(colors,sensor_right,p_move,p,measurements,motions)

#Your probability array must be printed 
#with the following code.

show(p)
