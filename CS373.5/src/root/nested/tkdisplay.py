from Tkinter import *
from math import *

# Do not modify path inside your function.
path=[[0, 0], #fix 
      [1, 0],
      [2, 0],
      [3, 0],
      [4, 0],
      [5, 0],
      [6, 0], #fix
      [6, 1],
      [6, 2],
      [6, 3], #fix
      [5, 3],
      [4, 3],
      [3, 3],
      [2, 3],
      [1, 3],
      [0, 3], #fix
      [0, 2],
      [0, 1]]

# Do not modify fix inside your function
fix = [1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0]

######################## ENTER CODE BELOW HERE #########################

def cxform(x):
    coo= [20*c+200 for c in x]
    return [c-2 for c in coo]+[c+2 for c in coo]


def iterate(newpath, fix, weight_smooth= 0.1, weight_data = 0.1):
    lp= len(newpath)
    gamma= 0.5*weight_smooth
    movement= 0.0
    for p in range(lp):
        if not fix[p]:
            for x in range(2):
                oldpos=newpath[p][x]
                newpath[p][x]+= weight_data*(path[p][x]-newpath[p][x])
                newpath[p][x]+= weight_smooth*(newpath[(p+1)%len(path)][x]+newpath[(p-1)%len(path)][x]-2*newpath[p][x])
                newpath[p][x]+= gamma*(2*newpath[(p-1)%lp][x]-newpath[(p-2)%lp][x]-newpath[p][x])
                newpath[p][x]+= gamma*(2*newpath[(p+1)%lp][x]-newpath[(p+2)%lp][x]-newpath[p][x])
                movement+= abs(newpath[p][x]-oldpos)
    print movement

def knopf():
    iterate(newpath, fix, 0.1, 0.0)
    for p, o in zip(newpath, npoints):
        apply(canvas.coords,[o]+cxform(p))

def smooth(path, fix, weight_data = 0.0, weight_smooth = 0.1, tolerance = 0.00001):
    newpath= [point[:] for point in path]
    lp= len(path)
    gamma= 0.5*weight_smooth
    while True:
        movement= 0.0
        for p in range(lp):
            if not fix[p]:
                for x in range(2):
                    oldpos=newpath[p][x]
                    newpath[p][x]+= gamma*(2*newpath[(p-1)%lp][x]-newpath[(p-2)%lp][x]-newpath[p][x])
                    newpath[p][x]+= gamma*(2*newpath[(p+1)%lp][x]-newpath[(p+2)%lp][x]-newpath[p][x])
                    movement+= abs(newpath[p][x]-oldpos)
        if movement < tolerance:
            break
    return newpath

#thank you - EnTerr - for posting this on our discussion forum


newpath= [point[:] for point in path]

master= Tk()
canvas= Canvas(master,width=500,height=500,name='kannwas')
ppoints= [canvas.create_oval(cxform(p)) for p in path]
npoints= [canvas.create_oval(cxform(p), fill="red") for p in path]
canvas.pack()
f= Frame(master,name='frahm')
b1= Button(f, text='Weiter so!', command= knopf)
#l= Label(f,text= 'Huhu',name='etikett')
b2= Button(f, text='Schulz jetzt!', command=f.quit)
f.pack()
b1.pack(side=LEFT)
b2.pack(side=LEFT)
#l.pack()
master.mainloop()
#print master.keys()
#print canvas.keys()
#print f.keys()
#print b1.keys()
#print b2.keys()


