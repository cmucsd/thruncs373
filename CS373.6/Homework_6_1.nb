(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     36847,       1091]
NotebookOptionsPosition[     35897,       1056]
NotebookOutlinePosition[     36509,       1080]
CellTagsIndexPosition[     36423,       1075]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"?", "SparseArray"}]], "Input",
 CellChangeTimes->{{3.542378238745225*^9, 3.542378241247637*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox["\<\"\\!\\(\\*RowBox[{\\\"SparseArray\\\", \\\"[\\\", \
RowBox[{\\\"{\\\", RowBox[{RowBox[{SubscriptBox[StyleBox[\\\"pos\\\", \
\\\"TI\\\"], StyleBox[\\\"1\\\", \\\"TR\\\"]], \\\"->\\\", \
SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"1\\\", \
\\\"TR\\\"]]}], \\\",\\\", RowBox[{SubscriptBox[StyleBox[\\\"pos\\\", \
\\\"TI\\\"], StyleBox[\\\"2\\\", \\\"TR\\\"]], \\\"->\\\", \
SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"2\\\", \
\\\"TR\\\"]]}], \\\",\\\", StyleBox[\\\"\[Ellipsis]\\\", \\\"TR\\\"]}], \\\"}\
\\\"}], \\\"]\\\"}]\\) yields a sparse array in which values \
\\!\\(\\*SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"i\\\", \
\\\"TI\\\"]]\\) appear at positions \
\\!\\(\\*SubscriptBox[StyleBox[\\\"pos\\\", \\\"TI\\\"], StyleBox[\\\"i\\\", \
\\\"TI\\\"]]\\). \\n\\!\\(\\*RowBox[{\\\"SparseArray\\\", \\\"[\\\", \
RowBox[{RowBox[{\\\"{\\\", RowBox[{SubscriptBox[StyleBox[\\\"pos\\\", \
\\\"TI\\\"], StyleBox[\\\"1\\\", \\\"TR\\\"]], \\\",\\\", \
SubscriptBox[StyleBox[\\\"pos\\\", \\\"TI\\\"], StyleBox[\\\"2\\\", \
\\\"TR\\\"]], \\\",\\\", StyleBox[\\\"\[Ellipsis]\\\", \\\"TR\\\"]}], \\\"}\\\
\"}], \\\"->\\\", RowBox[{\\\"{\\\", \
RowBox[{SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"1\\\", \
\\\"TR\\\"]], \\\",\\\", SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], \
StyleBox[\\\"2\\\", \\\"TR\\\"]], \\\",\\\", StyleBox[\\\"\[Ellipsis]\\\", \\\
\"TR\\\"]}], \\\"}\\\"}]}], \\\"]\\\"}]\\) yields the same sparse array. \
\\n\\!\\(\\*RowBox[{\\\"SparseArray\\\", \\\"[\\\", StyleBox[\\\"list\\\", \\\
\"TI\\\"], \\\"]\\\"}]\\) yields a sparse array version of \\!\\(\\*StyleBox[\
\\\"list\\\", \\\"TI\\\"]\\). \\n\\!\\(\\*RowBox[{\\\"SparseArray\\\", \
\\\"[\\\", RowBox[{StyleBox[\\\"data\\\", \\\"TI\\\"], \\\",\\\", \
RowBox[{\\\"{\\\", RowBox[{SubscriptBox[StyleBox[\\\"d\\\", \\\"TI\\\"], \
StyleBox[\\\"1\\\", \\\"TR\\\"]], \\\",\\\", SubscriptBox[StyleBox[\\\"d\\\", \
\\\"TI\\\"], StyleBox[\\\"2\\\", \\\"TR\\\"]], \\\",\\\", StyleBox[\\\"\
\[Ellipsis]\\\", \\\"TR\\\"]}], \\\"}\\\"}]}], \\\"]\\\"}]\\) yields a sparse \
array representing a \\!\\(\\*RowBox[{SubscriptBox[StyleBox[\\\"d\\\", \\\"TI\
\\\"], StyleBox[\\\"1\\\", \\\"TR\\\"]], \\\"\[Times]\\\", \
SubscriptBox[StyleBox[\\\"d\\\", \\\"TI\\\"], StyleBox[\\\"2\\\", \
\\\"TR\\\"]], \\\"\[Times]\\\", StyleBox[\\\"\[Ellipsis]\\\", \
\\\"TR\\\"]}]\\) array. \\n\\!\\(\\*RowBox[{\\\"SparseArray\\\", \\\"[\\\", \
RowBox[{StyleBox[\\\"data\\\", \\\"TI\\\"], \\\",\\\", StyleBox[\\\"dims\\\", \
\\\"TI\\\"], \\\",\\\", StyleBox[\\\"val\\\", \\\"TI\\\"]}], \\\"]\\\"}]\\) \
yields a sparse array in which unspecified elements are taken to have value \
\\!\\(\\*StyleBox[\\\"val\\\", \\\"TI\\\"]\\). \"\>", "MSG"], 
  "\[NonBreakingSpace]", 
  ButtonBox[
   StyleBox["\[RightSkeleton]", "SR"],
   Active->True,
   BaseStyle->"Link",
   ButtonData->"paclet:ref/SparseArray"]}]], "Print", "PrintUsage",
 CellChangeTimes->{3.542378241618052*^9},
 CellTags->"Info3542353041-2311093"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"1", ",", "1"}], "}"}], "\[Rule]", "1"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", "1", "}"}], "\[Rule]", "5"}], "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"1", ",", "1"}], "}"}], "\[Rule]", "2"}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"1", ",", "4"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"4", ",", "1"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"4", ",", "4"}], "}"}], "\[Rule]", "2"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "1", "}"}], "\[Rule]", 
          RowBox[{"2", " ", 
           RowBox[{"(", 
            RowBox[{"-", "2"}], ")"}]}]}], ",", 
         RowBox[{
          RowBox[{"{", "4", "}"}], "\[Rule]", 
          RowBox[{"2", " ", "2"}]}]}], "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"1", ",", "1"}], "}"}], "\[Rule]", "1"}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"1", ",", "2"}], "}"}], "\[Rule]", 
          RowBox[{"-", "1"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "1"}], "}"}], "\[Rule]", 
          RowBox[{"-", "1"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "2"}], "}"}], "\[Rule]", "1"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "1", "}"}], "\[Rule]", 
          RowBox[{"-", "7"}]}], ",", 
         RowBox[{
          RowBox[{"{", "2", "}"}], "\[Rule]", "7"}]}], "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "2"}], "}"}], "\[Rule]", "2"}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "5"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"5", ",", "2"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"5", ",", "5"}], "}"}], "\[Rule]", "2"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "2", "}"}], "\[Rule]", 
          RowBox[{"2", " ", 
           RowBox[{"(", 
            RowBox[{"-", "4"}], ")"}]}]}], ",", 
         RowBox[{
          RowBox[{"{", "5", "}"}], "\[Rule]", 
          RowBox[{"2", " ", "4"}]}]}], "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "2"}], "}"}], "\[Rule]", "1"}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"2", ",", "3"}], "}"}], "\[Rule]", 
          RowBox[{"-", "1"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"3", ",", "2"}], "}"}], "\[Rule]", 
          RowBox[{"-", "1"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"3", ",", "3"}], "}"}], "\[Rule]", "1"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "2", "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", "3", "}"}], "\[Rule]", "2"}]}], "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
   
   RowBox[{"{", 
    RowBox[{
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", 
           RowBox[{"3", ",", "3"}], "}"}], "\[Rule]", "2"}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"3", ",", "5"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"5", ",", "3"}], "}"}], "\[Rule]", 
          RowBox[{"-", "2"}]}], ",", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{"5", ",", "5"}], "}"}], "\[Rule]", "2"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"5", ",", "5"}], "}"}]}], "]"}], ",", "\[IndentingNewLine]", 
     RowBox[{"SparseArray", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"{", "3", "}"}], "\[Rule]", 
          RowBox[{"2", " ", 
           RowBox[{"(", 
            RowBox[{"-", "2"}], ")"}]}]}], ",", 
         RowBox[{
          RowBox[{"{", "5", "}"}], "\[Rule]", 
          RowBox[{"2", " ", "2"}]}]}], " ", "}"}], ",", 
       RowBox[{"{", "5", "}"}]}], "]"}]}], "}"}]}], 
  "}"}], "\[IndentingNewLine]", 
 RowBox[{"Map", "[", 
  RowBox[{"MatrixForm", ",", "%", ",", 
   RowBox[{"{", "2", "}"}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"Transpose", "[", "%%", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"Plus", "@@", "#"}], "&"}], ")"}], "/@", 
  "%"}], "\[IndentingNewLine]", 
 RowBox[{"MatrixForm", "/@", "%"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"Inverse", "[", 
     RowBox[{"#", "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}], 
     "]"}], ".", 
    RowBox[{"#", "\[LeftDoubleBracket]", "2", "\[RightDoubleBracket]"}]}], 
   "&"}], "[", "%%", "]"}]}], "Input",
 CellChangeTimes->{{3.54237801296203*^9, 3.542378195936728*^9}, {
  3.542378320709578*^9, 3.542378434999803*^9}, {3.542378532322572*^9, 
  3.542378730308461*^9}, {3.542378856533543*^9, 3.542378968166884*^9}, {
  3.542379032788481*^9, 3.542379108627929*^9}, {3.542379149954284*^9, 
  3.54237926036199*^9}, {3.542379310280334*^9, 3.542379345212991*^9}, {
  3.542379461170118*^9, 3.542379530332793*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "1", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 1, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "1", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 1, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.542379531661939*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"1", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {1, {{0, 1, 1, 1, 1, 1}, {{1}}}, {1}}]]]], ",", 
     
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {"5"},
          {"0"},
          {"0"},
          {"0"},
          {"0"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[Automatic, {5}, 0, {1, {{0, 1}, {{1}}}, {5}}]]]]}], "}"}],
    ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"2", "0", "0", 
          RowBox[{"-", "2"}], "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {
          RowBox[{"-", "2"}], "0", "0", "2", "0"},
         {"0", "0", "0", "0", "0"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {
         1, {{0, 2, 2, 2, 4, 4}, {{1}, {4}, {1}, {4}}}, {2, -2, -2, 2}}]]]], 
     ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {
           RowBox[{"-", "4"}]},
          {"0"},
          {"0"},
          {"4"},
          {"0"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[
        Automatic, {5}, 0, {1, {{0, 2}, {{1}, {4}}}, {-4, 4}}]]]]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"1", 
          RowBox[{"-", "1"}], "0", "0", "0"},
         {
          RowBox[{"-", "1"}], "1", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {
         1, {{0, 2, 4, 4, 4, 4}, {{1}, {2}, {1}, {2}}}, {1, -1, -1, 1}}]]]], 
     ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {
           RowBox[{"-", "7"}]},
          {"7"},
          {"0"},
          {"0"},
          {"0"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[
        Automatic, {5}, 0, {1, {{0, 2}, {{1}, {2}}}, {-7, 7}}]]]]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"0", "0", "0", "0", "0"},
         {"0", "2", "0", "0", 
          RowBox[{"-", "2"}]},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", 
          RowBox[{"-", "2"}], "0", "0", "2"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {
         1, {{0, 0, 2, 2, 2, 4}, {{2}, {5}, {2}, {5}}}, {2, -2, -2, 2}}]]]], 
     ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {"0"},
          {
           RowBox[{"-", "8"}]},
          {"0"},
          {"0"},
          {"8"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[
        Automatic, {5}, 0, {1, {{0, 2}, {{2}, {5}}}, {-8, 8}}]]]]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"0", "0", "0", "0", "0"},
         {"0", "1", 
          RowBox[{"-", "1"}], "0", "0"},
         {"0", 
          RowBox[{"-", "1"}], "1", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {
         1, {{0, 0, 2, 4, 4, 4}, {{2}, {3}, {2}, {3}}}, {1, -1, -1, 1}}]]]], 
     ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {"0"},
          {
           RowBox[{"-", "2"}]},
          {"2"},
          {"0"},
          {"0"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[
        Automatic, {5}, 0, {1, {{0, 2}, {{2}, {3}}}, {-2, 2}}]]]]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"(", "\[NoBreak]", GridBox[{
         {"0", "0", "0", "0", "0"},
         {"0", "0", "0", "0", "0"},
         {"0", "0", "2", "0", 
          RowBox[{"-", "2"}]},
         {"0", "0", "0", "0", "0"},
         {"0", "0", 
          RowBox[{"-", "2"}], "0", "2"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.7]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
       SparseArray[
        Automatic, {5, 5}, 0, {
         1, {{0, 0, 0, 2, 2, 4}, {{3}, {5}, {3}, {5}}}, {2, -2, -2, 2}}]]]], 
     ",", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {"0"},
          {"0"},
          {
           RowBox[{"-", "4"}]},
          {"0"},
          {"4"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[
        SparseArray[
        Automatic, {5}, 0, {1, {{0, 2}, {{3}, {5}}}, {-4, 4}}]]]]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.542379531665402*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "1", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 1, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "4", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 4, ">"],
         Editable->False], ",", 
        RowBox[{"{", 
         RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "1", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 1, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False], ",", 
     TagBox[
      RowBox[{"SparseArray", "[", 
       RowBox[{
        InterpretationBox[
         RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "2", 
          "\[InvisibleSpace]", "\<\">\"\>"}],
         SequenceForm["<", 2, ">"],
         Editable->False], ",", 
        RowBox[{"{", "5", "}"}]}], "]"}],
      False,
      Editable->False]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.54237953166822*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   TagBox[
    RowBox[{"SparseArray", "[", 
     RowBox[{
      InterpretationBox[
       RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "15", 
        "\[InvisibleSpace]", "\<\">\"\>"}],
       SequenceForm["<", 15, ">"],
       Editable->False], ",", 
      RowBox[{"{", 
       RowBox[{"5", ",", "5"}], "}"}]}], "]"}],
    False,
    Editable->False], ",", 
   TagBox[
    RowBox[{"SparseArray", "[", 
     RowBox[{
      InterpretationBox[
       RowBox[{"\<\"<\"\>", "\[InvisibleSpace]", "5", 
        "\[InvisibleSpace]", "\<\">\"\>"}],
       SequenceForm["<", 5, ">"],
       Editable->False], ",", 
      RowBox[{"{", "5", "}"}]}], "]"}],
    False,
    Editable->False]}], "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.542379531670069*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   TagBox[
    RowBox[{"(", "\[NoBreak]", GridBox[{
       {"4", 
        RowBox[{"-", "1"}], "0", 
        RowBox[{"-", "2"}], "0"},
       {
        RowBox[{"-", "1"}], "4", 
        RowBox[{"-", "1"}], "0", 
        RowBox[{"-", "2"}]},
       {"0", 
        RowBox[{"-", "1"}], "3", "0", 
        RowBox[{"-", "2"}]},
       {
        RowBox[{"-", "2"}], "0", "0", "2", "0"},
       {"0", 
        RowBox[{"-", "2"}], 
        RowBox[{"-", "2"}], "0", "4"}
      },
      GridBoxAlignment->{
       "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
        "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.7]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
    Function[BoxForm`e$, 
     MatrixForm[
     SparseArray[
      Automatic, {5, 5}, 0, {
       1, {{0, 3, 7, 10, 12, 15}, {{1}, {4}, {2}, {2}, {5}, {3}, {1}, {3}, {
         5}, {2}, {1}, {4}, {3}, {5}, {2}}}, {4, -2, -1, 4, -2, -1, -1, 
        3, -2, -1, -2, 2, -2, 4, -2}}]]]], ",", 
   TagBox[
    RowBox[{"(", "\[NoBreak]", 
     TagBox[GridBox[{
        {
         RowBox[{"-", "6"}]},
        {
         RowBox[{"-", "3"}]},
        {
         RowBox[{"-", "2"}]},
        {"4"},
        {"12"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
         "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.5599999999999999]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}],
      Column], "\[NoBreak]", ")"}],
    Function[BoxForm`e$, 
     MatrixForm[
      SparseArray[
      Automatic, {5}, 0, {
       1, {{0, 5}, {{1}, {2}, {4}, {3}, {5}}}, {-6, -3, 4, -2, 12}}]]]]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.542379531671212*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"5", ",", "12", ",", "14", ",", "7", ",", "16"}], "}"}]], "Output",
 CellChangeTimes->{
  3.542378730931362*^9, {3.542378955568515*^9, 3.542378968758257*^9}, 
   3.542379110341999*^9, 3.542379151362417*^9, {3.542379184219327*^9, 
   3.542379202908977*^9}, {3.542379240779493*^9, 3.542379260771071*^9}, 
   3.542379346449157*^9, {3.542379490539298*^9, 3.542379531672252*^9}}]
}, Open  ]]
},
WindowSize->{1089, 659},
WindowMargins->{{Automatic, 373}, {92, Automatic}},
ShowSelection->True,
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.5, 1.5 Inherited],
FrontEndVersion->"8.0 for Linux x86 (32-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "Info3542353041-2311093"->{
  Cell[702, 26, 3052, 48, 229, "Print",
   CellTags->"Info3542353041-2311093"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"Info3542353041-2311093", 36319, 1069}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 120, 2, 43, "Input"],
Cell[702, 26, 3052, 48, 229, "Print",
 CellTags->"Info3542353041-2311093"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3791, 79, 7190, 216, 449, "Input"],
Cell[10984, 297, 4950, 157, 179, "Output"],
Cell[15937, 456, 11248, 332, 256, "Output"],
Cell[27188, 790, 4793, 149, 125, "Output"],
Cell[31984, 941, 1042, 30, 43, "Output"],
Cell[33029, 973, 2436, 71, 127, "Output"],
Cell[35468, 1046, 413, 7, 43, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
