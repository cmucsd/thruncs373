# Write a program that will iteratively update and
# predict based on the location measurements 
# and inferred motions shown below. 

def update(mean1, var1, mean2, var2):
    new_mean = (var2 * mean1 + var1 * mean2) / (var1 + var2)
    new_var = 1/(1/var1 + 1/var2)
    return [new_mean, new_var]

def predict(mean1, var1, mean2, var2):
    new_mean = mean1 + mean2
    new_var = var1 + var2
    return [new_mean, new_var]

measurements = [5., 6., 7., 9., 10.]
motion = [1., 1., 2., 1., 1.]
measurement_sig = 4.
motion_sig = 2.
mu = 0
sig = 10000

#Please print out ONLY the final values of the mean
#and the variance in a list [mu, sig]. 

# Insert code here
def motu_proprio(position, ort_bewegung):
    ort= ort_bewegung[0]
    bewegung= ort_bewegung[1]
    [hier,hier_var]= update(position[0],position[1], ort[0],ort[1])
    return predict(hier,hier_var, bewegung[0],bewegung[1])

[mu, sig]= reduce(motu_proprio, 
    [[[ort,measurement_sig],[bewegung,motion_sig]] 
        for [ort,bewegung] in zip(measurements,motion)],
    [mu,sig])

print [mu, sig]